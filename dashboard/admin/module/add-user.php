<?php 
if ($_GET['page']='input-user'){
   
?>
          <div class="row">
                <div class="col-lg-12">
					<h3 class="page-header"><strong>Tambah Pengguna</strong></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Pengguna
                        </div>
                     
                         <form action="index.php?page=proses&act=input-user" class="form-horizontal"   method="post" role="form">
                        <div class="panel-body">
                          
                                <div class="form-group">
                                    <label class="control-label col-sm-1">Nim</label>
                                    <div class="col-md-6">
                                        <input required class="form-control col-md-9"  placeholder="Nim" name="nim" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-1">Nama</label>
                                    <div class="col-md-6">
                                        <input required class="form-control" placeholder="Nama" name="nama" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-1">Password</label>
                                    <div class="col-md-6">
                                        <input required  type="password" class="form-control"  placeholder="Password" name="password"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-1">Email</label>
                                    <div class="col-md-6">
                                        <input required  type="email" class="form-control"  placeholder="Email" name="email" />
                                    </div>
                                </div>
                            
                        </div>

                        <div class="panel-footer">
                        
                        <button type="submit" class="btn btn-default">SIMPAN</button>   
                        </div>
                         </form>      
                               
                               

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
<?php
}
?>